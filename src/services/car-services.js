const API_BASE_URL = "https://api.api-ninjas.com/v1/cars";
const API_KEY = "dhLV18q2fiD1QMjcO8CXPQ==S0yDflJBiEQTTY5e";

export const getCarData = async (options) => {
    try {
      const queryParams = new URLSearchParams(options).toString();  // Convert the options object to a query string
      const response = await fetch(`${API_BASE_URL}?${queryParams}`, {
        headers: {
          'X-Api-Key': API_KEY,
        }
      });
      const data = await response.json();
      return data;
    } catch (error) {
      console.error("Error fetching car data:", error);
      return null;
    }
  };
  
