import { get, set, update , ref, query, equalTo, orderByChild } from 'firebase/database';
import { getDatabase} from "firebase/database";
import { db} from '../firebase/firebase-config';


const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val();

  return Object.keys(usersDocument).map((key) => {
    const user = usersDocument[key];

    return {
      ...user,
      id: key
    };
  });
};

export const getAllUsers = async () => {
  return get(ref(db, 'users')).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromUsersDocument(snapshot);
  });
};
 
export const getUserById = async (uid) => {
  const snapshot = await get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
  if (!snapshot.exists()) {
    return {};
  }

  // eslint-disable-next-line no-unsafe-optional-chaining
  const { ...value } = snapshot.val()?.[Object.keys(snapshot.val())?.[0]];

  return {
    ...value
  };
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};


/**
 * Retrieves user data by the username.
 */
export const getUser = async (username) => {
    const snapshot = await get(ref(db, `users/${username}`));

    return snapshot.val();
};

export const getUserByUsername = (username) => {
  return get(ref(db, `users/${username}`));
};




/**
 * Creates a new user in the Firebase Realtime Database.
 */
export const createUser = async (
    uid,
    email,
    handle,
    firstname,
    lastname,
    phoneNumber,
  ) => {
    const userData = {
      uid,
      email,
      handle,
      firstname,
      lastname,
      phoneNumber,
      registeredOn: Date.now(),
      role: 'user',
      favorites: {},
    };
  
    await set(ref(db, `users/${handle}`), userData);
  
    return {
      ...userData,
    };
  };



  export const updateUserRole = (username, role) => {
    return update(ref(db), {
      [`users/${username}/role`]: role,
    });
  };
  
  export const updateUserPhone = (username, phone) => {
    return update(ref(db), {
      [`users/${username}/phone`]: phone,
    });
  };
  
  export const updateUserProfilePicture = (username, url) => {
    return update(ref(db), {
      [`users/${username}/avatar`]: url,
    });
  };
  
  export const updateUserEmail = (username, email) => {
    return update(ref(db), {
      [`users/${username}/email`]: email,
    });
  };
  
  export const updateUserFirstName = (username, firstName) => {
    return update(ref(db), {
      [`users/${username}/firstname`]: firstName,
    });
  };
  
  export const updateUserLastName = (username, lastName) => {
    return update(ref(db), {
      [`users/${username}/lastname`]: lastName,
    });
  };
  
  export const updateUserStatus = (username, status) => {
    return update(ref(db), {
      [`users/${username}/status`]: status,
    });
  };

 export async function fetchUsers() {
    const db = getDatabase();
    const usersRef = ref(db, "users/");
    const snapshot = await get(usersRef);
    if (snapshot.exists()) {
      const data = snapshot.val();
      return Object.values(data || {});
    } else {
      return [];
    }
  }