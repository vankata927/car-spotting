import {createUserWithEmailAndPassword, signInWithEmailAndPassword} from 'firebase/auth'
import { authenticate } from '../firebase/firebase-config';

export const registerUser = (email, password) => {
  return createUserWithEmailAndPassword(authenticate, email, password)
}

export const loginUser = async (email, password) => {
  try {
    const userCredential = await signInWithEmailAndPassword(authenticate, email, password);
    const user = userCredential.user;
    return { email: user.email, uid: user.uid };
  } catch (error) {
    console.error(error);
    throw error;
  }
};
export const handleLogout = async (logout, navigate) => {
  try {
    await logout();
    navigate("/login");
  } catch (error) {
    console.error("Logout failed", error);
  }
};
