import { db, storage } from "../firebase/firebase-config";
import { ref, remove, get, update } from "firebase/database";
import { deleteObject, ref as storageRef } from "firebase/storage";

export const fetchUserPhotos = async (handle) => {
  const photosRef = ref(db, `photos`);
  const snapshot = await get(photosRef);

  if (snapshot.exists()) {
    const allPhotos = [];

    // Loop over each photo and capture both the key and the value
    snapshot.forEach(childSnapshot => {
      const photo = childSnapshot.val();
      photo.firebaseKey = childSnapshot.key; // Add the key as firebaseKey
      allPhotos.push(photo);
    });

    return allPhotos.filter(photo => photo.spotter === handle);
  }

  return [];
};

export const handleDeletePhoto = async (photo, uid) => {
  try {
    // Delete from Firebase Storage
    const photoStorageRef = storageRef(storage, `photos/${uid}/${photo.id}`);
    await deleteObject(photoStorageRef);

    // Use the firebaseKey to delete from Firebase Realtime Database
    if (!photo.firebaseKey) {
      console.error("Firebase key missing. Cannot delete from Realtime Database.");
      return false;
    }
    const photoDatabaseRef = ref(db, `photos/${photo.firebaseKey}`);
    await remove(photoDatabaseRef);

    return true;
  } catch (error) {
    console.error("Error deleting photo:", error);
    return false;
  }
};

export const addPhotoToFavorites = (username, firebaseKey) => {
  return update(ref(db), {
    [`users/${username}/favorites/${firebaseKey}`]: true
  });
};
export const removePhotoFromFavorites = (username, photoId) => {
  return remove(ref(db, `users/${username}/favorites/${photoId}`));
};




