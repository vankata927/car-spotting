
import { ref, onValue } from "firebase/database";
import { db } from "../firebase/firebase-config";

export const isUsernameValid = (username) => {
  return username.length >= 2 && username.length <= 20;
};
export const isFirstNameValid = (firstname) => {
    const firstnameRegex = /^.{2,10}$/;
    return firstnameRegex.test(firstname);
  };
  
  export const isLastNameValid = (lastname) => {
    const lastnameRegex = /^.{2,10}$/;
    return lastnameRegex.test(lastname);
  };

export const isEmailValid = (email) => {
  const emailRegex = /\S+@\S+\.\S+/;
  return emailRegex.test(email);
};
export const isPasswordValid = (password) => {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return passwordRegex.test(password);
  };

export const isPhoneNumberValid = (phoneNumber) => {
  const phoneNumberRegex = /^\d{10}$/;
  return phoneNumberRegex.test(phoneNumber);
};

export const usersRef = ref(db, "users");

export const fetchUserEmails = () => {
  return new Promise((resolve) => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const userEmails = Object.keys(data)
          .map((key) => data[key].email)
          .filter((email) => email);

        resolve(userEmails);
 } else {
        resolve([]); // Resolve with an empty array if no users are found
      }
    });
  });
};

export const checkEmailExists = async (email) => {
  const userEmails = await fetchUserEmails();
  return userEmails.includes(email);
};

export const fetchUserPhoneNumbers = () => {
  return new Promise((resolve) => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const userPhoneNumbers = Object.keys(data)
          .map((key) => data[key].phoneNumber)
          .filter((phoneNumber) => phoneNumber); // This filters out any undefined or empty values

        resolve(userPhoneNumbers);
      } else {
        resolve([]); // Resolve with an empty array if no users or phone numbers are found
      }
    });
  });
};

export const checkPhoneNumberExists = async (phoneNumber) => {
  const userPhoneNumbers = await fetchUserPhoneNumbers();
  return userPhoneNumbers.includes(phoneNumber);
};

export const isAgeValid = (age) => {
  const ageNumber = Number(age);
  return ageNumber >= 1 && ageNumber <= 130 && !isNaN(ageNumber);
};

export const isWeightValid = (weight) => {
  const weightNumber = Number(weight);
  return weightNumber >= 20 && weightNumber <= 400 && !isNaN(weightNumber);
};

export const isHeightValid = (height) => {
  const heightNumber = Number(height);
  return heightNumber >= 50 && heightNumber <= 250 && !isNaN(heightNumber);
};