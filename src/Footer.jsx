import { Typography, Box } from "@mui/material";

const Footer = () => {
  function Copyright() {
    return (
      <Typography variant="body2" color="text.secondary" align="center">
        {"Copyright © "}
        Car Spotting
        {new Date().getFullYear()}
        {"."}
      </Typography>
    );
  }
  return (
    <>
      <Box sx={{ bgcolor: "background.paper", p: 6 }} component="footer">
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          Powered by ReactJS, MUI, and Firebase
        </Typography>
        <Copyright />
      </Box>
    </>
  );
};

export default Footer;
