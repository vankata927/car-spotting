
import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';


const firebaseConfig = {
    apiKey: "AIzaSyBcgUwVf9Cd_sPt6abTnLE9mdb4Cn-LjC4",
    authDomain: "carspotting-1b722.firebaseapp.com",
    databaseURL: "https://carspotting-1b722-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "carspotting-1b722",
    storageBucket: "carspotting-1b722.appspot.com",
    messagingSenderId: "33418363414",
    appId: "1:33418363414:web:2769c8dd04bc93aead2b37"
};

export const app = initializeApp(firebaseConfig);

export const authenticate = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
