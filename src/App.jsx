import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppContext from "./contexts/AppContext";
import Navbar from "./components/Navbar";
import { authenticate } from "./firebase/firebase-config";
import { useState, useEffect } from "react";
import { getUserById } from "./services/user-services";
import { signOut } from "firebase/auth";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import HomeView from "./components/Home";
import About from "./components/About/About";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import Loader from "./components/Loader";
import VerifyEmail from "./components/VerifyEmail";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import Photos from "./components/Photos/Photos";
import UploadPhoto from "./components/Photos/UploadPhoto";
import EditProfile from "./components/UserProfile/EditProfile";
import ProfileView from "./components/UserProfile/ProfileView";
import Favorites from "./components/Photos/Favorites";
import CarData from "./components/CarData";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
    isVerified: false,
  });
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const unsubscribe = authenticate.onAuthStateChanged((user) => {
      setIsLoading(true);
      if (user) {
        setAppState((prevState) => ({
          ...prevState,
          user: {
            email: user.email,
            uid: user.uid,
          },
        }));
        getUserById(user.uid)
          .then((userData) => {
            setAppState((prevState) => ({
              ...prevState,
              userData: userData,
            }));
            setIsLoading(false);
          })
          .catch((e) => {
            console.error(e.message);
            setIsLoading(false);
          });
      } else {
        setAppState((prevState) => ({
          ...prevState,
          user: null,
          userData: null,
        }));
        setIsLoading(false);
      }
    });

    return () => unsubscribe();
  }, []);

  const logout = async () => {
    try {
      await signOut(authenticate);
      setAppState((prevState) => ({
        ...prevState,
        user: null,
        userData: null,
        isVerified: false,
      }));
    } catch (error) {
      console.error("Logout failed", error);
    }
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <AppContext.Provider value={{ appState, setAppState, logout }}>
      <ThemeProvider theme={darkTheme} />

      <Router>
        <Navbar />
        <Routes>
          <Route
            path="/user/:handle"
            element={
              <ProtectedRoute>
                <ProfileView />
              </ProtectedRoute>
            }
          />

          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/" element={<HomeView />} />
          <Route path="/about" element={<About />} />
          <Route path="/verify-email" element={<VerifyEmail />} />

          <Route
            path="/photos"
            element={
              appState.user && (
                <ProtectedRoute>
                  <Photos />
                </ProtectedRoute>
              )
            }
          />
          <Route
            path="/upload"
            element={
              appState.user && (
                <ProtectedRoute>
                  <UploadPhoto />
                </ProtectedRoute>
              )
            }
          />
          <Route
            path="/edit-profile"
            element={
              appState.user && (
                <ProtectedRoute>
                  <EditProfile />
                </ProtectedRoute>
              )
            }
          />
          <Route
            path="/data"
            element={
              appState.user && (
                <ProtectedRoute>
                  <CarData />
                </ProtectedRoute>
              )
            }
          />
          <Route
            path={
              appState.userData
                ? `users/${appState.userData.handle}/favorites`
                : "#"
            }
            element={
              appState.user && (
                <ProtectedRoute>
                  <Favorites />
                </ProtectedRoute>
              )
            }
          />
        </Routes>
      </Router>
    </AppContext.Provider>
  );
}

export default App;
