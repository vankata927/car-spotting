import { useState } from "react";
import { getCarData } from "../services/car-services";
import {
  TextField,
  Button,
  Typography,
  Box,
  Select,
  MenuItem,
} from "@mui/material";

function CarData() {
  const [searchParams, setSearchParams] = useState({
    model: "",
    make: "",
    fuel_type: "",
    drive: "",
    transmission: "",
    year: "",
    // ... add more parameters if needed
  });
  const [carData, setCarData] = useState([]);

  const handleFetchCarData = async () => {
    const data = await getCarData(searchParams);
    console.log("API Response:", data); // Log the data for inspection
    setCarData(data);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSearchParams((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <Box
      sx={{
        mt: 3,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Typography variant="h4">Search Car Data</Typography>
      <TextField
        name="model"
        variant="outlined"
        label="Model"
        value={searchParams.model}
        onChange={handleChange}
        fullWidth
        margin="normal"
      />
      <TextField
        name="make"
        variant="outlined"
        label="Make"
        value={searchParams.make}
        onChange={handleChange}
        fullWidth
        margin="normal"
      />
      <Select
        name="fuel_type"
        value={searchParams.fuel_type}
        onChange={handleChange}
        fullWidth
      >
        <MenuItem value="gas">Gas</MenuItem>
        <MenuItem value="diesel">Diesel</MenuItem>
        <MenuItem value="electricity">Electricity</MenuItem>
      </Select>
      <Select
        name="drive"
        value={searchParams.drive}
        onChange={handleChange}
        fullWidth
      >
        <MenuItem value="fwd">FWD</MenuItem>
        <MenuItem value="rwd">RWD</MenuItem>
        <MenuItem value="awd">AWD</MenuItem>
        <MenuItem value="4wd">4WD</MenuItem>
      </Select>
      {/* ... Add more fields as needed */}
      <Button variant="contained" color="primary" onClick={handleFetchCarData}>
        Search
      </Button>
      {carData.length > 0 && (
        <Box sx={{ mt: 3 }}>
          <Typography variant="h5">Car Details:</Typography>
          {carData.map((car, index) => (
            <Box key={index} sx={{ mt: 2 }}>
              <Typography variant="body1">Model: {car.model}</Typography>
              <Typography variant="body1">Make: {car.make}</Typography>
              <Typography variant="body1">
                Fuel Type: {car.fuel_type}
              </Typography>
              <Typography variant="body1">Drive: {car.drive}</Typography>
              <Typography variant="body1">
                Transmission: {car.transmission}
              </Typography>
              <Typography variant="body1">Year: {car.year}</Typography>
              {/* ... display car details */}
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
}

export default CarData;
