import { useContext, useState, useEffect } from "react";
import { ref, get } from "firebase/database";
import { Box, Paper, Typography, Grid, Button } from "@mui/material";
import AppContext from "../../contexts/AppContext";
import ImageModal from "./ImageModal";
import { db } from "../../firebase/firebase-config";
import "./Photos.css";
import { fetchUserPhotos } from "../../services/photo-services";
import { removePhotoFromFavorites } from "../../services/photo-services";

function Favorites() {
  const { appState } = useContext(AppContext);
  const [favorites, setFavorites] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentImage, setCurrentImage] = useState("");
  const [currentDescription, setCurrentDescription] = useState("");

  useEffect(() => {
    const fetchFavorites = async () => {
      const favoritesRef = ref(
        db,
        `users/${appState.userData.handle}/favorites`
      );
      const favoritesSnapshot = await get(favoritesRef);
      if (favoritesSnapshot.exists()) {
        const favoritesData = favoritesSnapshot.val();
        const favoritePhotoStorageIds = Object.keys(favoritesData); // Get storage IDs of the favorites

        const allPhotos = await fetchUserPhotos(appState.userData.handle);

        // Filter photos that have their ID in the list of favoritePhotoStorageIds
        const favoritePhotos = allPhotos.filter((photo) =>
          favoritePhotoStorageIds.includes(String(photo.id))
        );

        setFavorites(favoritePhotos);
      }
    };

    fetchFavorites();
  }, [appState.userData.handle]);

  const handleImageClick = (imageUrl, description) => {
    setCurrentImage(imageUrl);
    setCurrentDescription(description);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setCurrentImage("");
  };
  const handleRemoveFromFavorites = async (photoId) => {
    try {
      await removePhotoFromFavorites(appState.userData.handle, photoId);
      // Update the UI by filtering out the removed photo from the favorites array
      setFavorites((prevFavorites) =>
        prevFavorites.filter((photo) => photo.id !== photoId)
      );
    } catch (error) {
      console.error("Error removing from favorites:", error);
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mt: 3,
      }}
    >
      <Typography variant="h4" gutterBottom>
        My Favorites
      </Typography>
      <Grid container spacing={3}>
        {favorites.map((photo) => (
          <Grid item xs={12} sm={4} key={photo.photoURL}>
            <Paper
              elevation={3}
              className="photo-card"
              sx={{ width: "100%", marginBottom: 2 }}
            >
              <Typography variant="h5" gutterBottom className="photo-header">
                {photo.headline}
              </Typography>
              <Box className="photo-content">
                <Box
                  className="photo-container"
                  onClick={() =>
                    handleImageClick(photo.photoURL, photo.description)
                  }
                >
                  <img
                    src={photo.photoURL}
                    alt={photo.headline}
                    className="photo-image"
                  />
                </Box>
                <Typography variant="body1">
                  <strong>Spotted by:</strong> {photo.spotter}
                </Typography>
                <Typography variant="body1">
                  <strong>Date:</strong>{" "}
                  {new Date(photo.date).toLocaleDateString()}
                </Typography>
                <Typography variant="body1">
                  <strong>Location:</strong> {photo.location}
                </Typography>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleRemoveFromFavorites(photo.id)}
                >
                  Remove from Favorites
                </Button>
              </Box>
            </Paper>
          </Grid>
        ))}
      </Grid>
      <ImageModal
        open={open}
        handleClose={handleClose}
        imageUrl={currentImage}
        description={currentDescription}
      />
    </Box>
  );
}

export default Favorites;
