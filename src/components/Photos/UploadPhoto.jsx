import { useContext, useState } from "react";
import AppContext from "../../contexts/AppContext";
import { useNavigate } from "react-router-dom";

import {
  ref as storageRef,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";

import { ref, push, set } from "firebase/database";
import { Box, Paper, Typography, TextField, Button } from "@mui/material";
import { db, storage } from "../../firebase/firebase-config";
function UploadPhoto() {
  const { appState } = useContext(AppContext);
  const [image, setImage] = useState(null);
  const [headline, setHeadline] = useState("");
  const [location, setLocation] = useState("");
  const [description, setDescription] = useState("");
  const [uploading, setUploading] = useState(false);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImage(file);
    }
  };

  const handleUpload = async () => {
    if (!image || !headline || !location) {
      setMessage("Please fill all fields and select an image.");
      return;
    }

    const photoId = Date.now(); // Unique ID for the photo
    const photoName = image.name; // Extract the file name

    const storageReference = storageRef(
      storage,
      `photos/${appState.user.uid}/${photoId}`
    );
    const uploadTask = uploadBytesResumable(storageReference, image);

    setUploading(true);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // You can add progress functionality here
      },
      (error) => {
        console.error("Error uploading image:", error);
        setMessage("Error uploading image. Please try again.");
        setUploading(false);
      },
      async () => {
        const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
        const photoData = {
          id: photoId,
          name: photoName,
          headline,
          spotter: appState.userData.handle, // Store the handle instead of UID
          date: new Date().toISOString(),
          location,
          description,
          photoURL: downloadURL,
        };
        const newPhotoRef = push(ref(db, "photos"));
        await set(newPhotoRef, photoData);

        photoData.firebaseKey = newPhotoRef.key;
        console.log("Stored firebaseKey:", photoData.firebaseKey);

        setUploading(false);
        setMessage("Photo uploaded successfully!");
        navigate("/photos");
      }
    );
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mt: 3,
      }}
    >
      <Paper elevation={3} sx={{ padding: 3, width: "80%", maxWidth: 500 }}>
        <Typography variant="h4" gutterBottom>
          Upload Photo
        </Typography>
        <TextField
          variant="outlined"
          label="Headline"
          value={headline}
          onChange={(e) => setHeadline(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          variant="outlined"
          label="Spotted In"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          variant="outlined"
          label="Description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          fullWidth
          margin="normal"
          multiline
          rows={4}
        />
        <TextField
          variant="outlined"
          type="file"
          onChange={handleImageChange}
          fullWidth
          margin="normal"
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleUpload}
          disabled={uploading}
        >
          {uploading ? "Uploading..." : "Upload Photo"}
        </Button>
        {message && (
          <Typography
            variant="body1"
            style={{ marginTop: "15px", color: "red" }}
          >
            {message}
          </Typography>
        )}
      </Paper>
    </Box>
  );
}

export default UploadPhoto;
