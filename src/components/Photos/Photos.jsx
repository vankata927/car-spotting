import { useState, useEffect, useContext } from "react";
import { ref, get } from "firebase/database";
import { Box, Paper, Typography, Button } from "@mui/material";
import { db } from "../../firebase/firebase-config";
import Grid from "@mui/material/Grid";
import ImageModal from "./ImageModal";
import AppContext from "../../contexts/AppContext";
import { addPhotoToFavorites } from "../../services/photo-services";
import "./Photos.css";

function Photos() {
  const { appState } = useContext(AppContext);
  const [photos, setPhotos] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentImage, setCurrentImage] = useState("");
  const [currentDescription, setCurrentDescription] = useState("");

  useEffect(() => {
    const fetchPhotos = async () => {
      const photosRef = ref(db, "photos");
      const snapshot = await get(photosRef);
      if (snapshot.exists()) {
        let photoList = Object.values(snapshot.val());

        // Fetch usernames for each photo
        for (let photo of photoList) {
          const userRef = ref(db, `users/${photo.spotter}`);
          const userSnapshot = await get(userRef);
          if (userSnapshot.exists()) {
            photo.spotter = userSnapshot.val().handle || photo.spotter;
          }
        }
        setPhotos(photoList);
      }
    };

    fetchPhotos();
  }, []);

  const handleImageClick = (imageUrl, description) => {
    setCurrentImage(imageUrl);
    setCurrentDescription(description);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setCurrentImage("");
  };
  const handleAddToFavorites = async (photoId) => {
    try {
      await addPhotoToFavorites(appState.userData.handle, photoId);
      // Optionally, you can give feedback to the user, e.g., a message or toast.
      alert("Photo added to favorites!");
    } catch (error) {
      console.error("Error adding to favorites:", error);
      alert("Failed to add photo to favorites. Please try again.");
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mt: 3,
      }}
    >
      <Grid container spacing={3}>
        {photos.map((photo) => (
          <Grid item xs={12} sm={4} key={photo.photoURL}>
            <Paper
              elevation={3}
              className="photo-card"
              sx={{ width: "100%", marginBottom: 2 }}
            >
              <Typography variant="h5" gutterBottom className="photo-header">
                {photo.headline}
              </Typography>
              <Box className="photo-content">
                <Box
                  className="photo-container"
                  onClick={() =>
                    handleImageClick(photo.photoURL, photo.description)
                  }
                >
                  <img
                    src={photo.photoURL}
                    alt={photo.headline}
                    className="photo-image"
                  />
                </Box>
                <Typography variant="body1">
                  <strong>Spotted by:</strong> {photo.spotter}
                </Typography>
                <Typography variant="body1">
                  <strong>Date:</strong>{" "}
                  {new Date(photo.date).toLocaleDateString()}
                </Typography>
                <Typography variant="body1">
                  <strong>Location:</strong> {photo.location}
                </Typography>
                <Button onClick={() => handleAddToFavorites(photo.id)}>
                  Add to Favorites
                </Button>
              </Box>
            </Paper>
          </Grid>
        ))}
      </Grid>
      <ImageModal
        open={open}
        handleClose={handleClose}
        imageUrl={currentImage}
        description={currentDescription}
      />
    </Box>
  );
}

export default Photos;
