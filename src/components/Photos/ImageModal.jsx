import { Modal, Box, Typography } from "@mui/material";
import PropTypes from "prop-types";

function ImageModal({ open, handleClose, imageUrl, description }) {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="image-modal-title"
      aria-describedby="image-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
        }}
      >
        <img
          src={imageUrl}
          alt="Full Size"
          style={{ width: "100%", height: "auto" }}
        />
        <Typography variant="body1" style={{ marginTop: "10px" }}>
          {description}
        </Typography>
      </Box>
    </Modal>
  );
}

ImageModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  imageUrl: PropTypes.string,
  description: PropTypes.string,
};

export default ImageModal;
