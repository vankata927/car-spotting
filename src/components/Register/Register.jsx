import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { getUser, createUser } from "../../services/user-services";
import { registerUser } from "../../services/auth-services";
import {
  isUsernameValid,
  isEmailValid,
  isPhoneNumberValid,
  checkEmailExists,
  checkPhoneNumberExists,
  isPasswordValid,
  isFirstNameValid,
  isLastNameValid,
} from "../../common/validations";
import { sendEmailVerification } from "firebase/auth";
import RegisterView from "./RegisterView";

const Register = () => {
  const navigate = useNavigate();

  const [form, setForm] = useState({
    username: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    email: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    firstname: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    lastname: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    password: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    phoneNumber: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
  });

  const updateUsername = (value = "") => {
    setForm({
      ...form,
      username: {
        value,
        touched: true,
        valid: isUsernameValid(value),
        error: !isUsernameValid(value)
          ? "Invalid username. Username must be unique and between 2 and 20 symbols."
          : "",
      },
    });
  };

  const updateEmail = async (value = "") => {
    setForm({
      ...form,
      email: {
        value,
        touched: true,
        valid: isEmailValid(value) && !checkEmailExists(value),
        error: !isEmailValid(value)
          ? "Invalid email format"
          : checkEmailExists(value)
          ? "Email already in use!"
          : "",
      },
    });

    if (isEmailValid(value)) {
      const emailExists = await checkEmailExists(value);

      if (emailExists) {
        setForm((prevForm) => ({
          ...prevForm,
          email: {
            ...prevForm.email,
            valid: false,
            error: "Email already exists",
          },
        }));
      } else {
        setForm((prevForm) => ({
          ...prevForm,
          email: {
            ...prevForm.email,
            valid: true,
            error: "",
          },
        }));
      }
    }
  };

  const updatePhoneNumber = async (value = "") => {
    setForm({
      ...form,
      phoneNumber: {
        value,
        touched: true,
        valid: isPhoneNumberValid(value) && !checkPhoneNumberExists(value),
        error: !isPhoneNumberValid(value)
          ? "Invalid phone number format"
          : checkPhoneNumberExists(value)
          ? "Phone number already in use!"
          : "",
      },
    });

    if (isPhoneNumberValid(value)) {
      const phoneNumberExists = await checkPhoneNumberExists(value);

      if (phoneNumberExists) {
        setForm((prevForm) => ({
          ...prevForm,
          phoneNumber: {
            ...prevForm.phoneNumber,
            valid: false,
            error: "Phone number already exists",
          },
        }));
      } else {
        setForm((prevForm) => ({
          ...prevForm,
          phoneNumber: {
            ...prevForm.phoneNumber,
            valid: true,
            error: "",
          },
        }));
      }
    }
  };

  const updateFirstName = (value = "") => {
    const isValid = isFirstNameValid(value);
    setForm({
      ...form,
      firstname: {
        value,
        touched: true,
        valid: isValid,
        error: !isValid
          ? "First name must be between 2 and 10 characters."
          : "",
      },
    });
  };

  const updateLastName = (value = "") => {
    const isValid = isLastNameValid(value);
    setForm({
      ...form,
      lastname: {
        value,
        touched: true,
        valid: isValid,
        error: !isValid ? "Last name must be between 2 and 10 characters." : "",
      },
    });
  };

  const updatePassword = (value = "") => {
    const isValid = isPasswordValid(value);
    setForm({
      ...form,
      password: {
        value,
        touched: true,
        valid: isValid,
        error: !isValid
          ? "Password must be at least 8 characters long, include at least one uppercase letter, one lowercase letter, one number, and one special character."
          : "",
      },
    });
  };

  const addUser = async (
    username,
    email,
    firstname,
    lastname,
    password,
    phoneNumber
  ) => {
    try {
      const user = await getUser(username);
      if (user !== null) {
        return console.log("username already exist");
      }
      const credentials = await registerUser(email, password);

      try {
        await sendEmailVerification(credentials.user);
        await createUser(
          credentials.user.uid,
          email,
          username,
          firstname,
          lastname,
          phoneNumber
        );
        navigate("/dashboard");
      } catch (error) {
        console.error(error.message);
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  const helper = async () => {
    await addUser(
      form.username.value,
      form.email.value,
      form.firstname.value,
      form.lastname.value,
      form.password.value,
      form.phoneNumber.value
    );
  };

  return (
    <RegisterView
      form={form}
      updateUsername={updateUsername}
      updateEmail={updateEmail}
      updatePhoneNumber={updatePhoneNumber}
      updateFirstName={updateFirstName}
      updateLastName={updateLastName}
      updatePassword={updatePassword}
      helper={helper}
    />
  );
};

export default Register;
