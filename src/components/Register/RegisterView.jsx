import PropTypes from "prop-types";
import {
  FormControl,
  FormLabel,
  TextField,
  Button,
  Container,
  Typography,
} from "@mui/material";
import Box from "@mui/material/Box";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import { Helmet } from "react-helmet-async";
import './RegisterView.css'

function RegisterView({
  form,
  updateUsername,
  updateEmail,
  updatePhoneNumber,
  updateFirstName,
  updateLastName,
  updatePassword,
  helper,
}) {
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const fieldVariant = isSmallScreen ? "standard" : "outlined";

  return (
    <>
      <Helmet>
        <title>Sign-up</title>
        <body className="sign-up-page" />
      </Helmet>
      <Container
        component="main"
        maxWidth='xs'>
        <Box
          sx={{
            marginTop: '90px', display: 'flex', flexDirection: 'column',
            alignItems: 'center', backgroundColor: 'white',
            padding: '60px 80px 40px 80px', borderRadius: '5px', paddingTop: '60px', marginBottom: '40px'
          }}>
          {/* <Box
            paddingTop={{ xs: "56px", sm: "64px" }}
            paddingX={{ xs: 2, sm: 3, md: 0 }}
            minHeight="100vh"
          > */}
          <Typography component="h1" variant="h5" marginBottom='20px'>
            Sign up
          </Typography>

          {/* <Stack spacing={fieldSpacing} alignItems="stretch"> */}
          <FormControl variant={fieldVariant}>
            <FormLabel>Username</FormLabel>
            <TextField
              type="text"
              value={form.username.value}
              onChange={(event) => updateUsername(event.target.value)}
              error={form.username.touched && !form.username.valid}
              helperText={
                form.username.touched && !form.username.valid
                  ? form.username.error
                  : ""
              }
            />
          </FormControl>
          <FormControl variant={fieldVariant}>
            <FormLabel>Email address</FormLabel>
            <TextField
              type="email"
              value={form.email.value}
              onChange={(event) => updateEmail(event.target.value)}
              error={form.email.touched && !form.email.valid}
              helperText={
                form.email.touched && !form.email.valid
                  ? form.email.error
                  : ""
              }
            />
          </FormControl>
          <FormControl variant={fieldVariant}>
            <FormLabel>First name</FormLabel>
            <TextField
              type="text"
              value={form.firstname.value}
              onChange={(event) => updateFirstName(event.target.value)}
              error={form.firstname.touched && !form.firstname.valid}
              helperText={
                form.firstname.touched && !form.firstname.valid
                  ? form.firstname.error
                  : ""
              }
            />
          </FormControl>
          <FormControl variant={fieldVariant}>
            <FormLabel>Last name</FormLabel>
            <TextField
              type="text"
              value={form.lastname.value}
              onChange={(event) => updateLastName(event.target.value)}
              error={form.lastname.touched && !form.lastname.valid}
              helperText={
                form.lastname.touched && !form.lastname.valid
                  ? form.lastname.error
                  : ""
              }
            />
          </FormControl>
          <FormControl variant={fieldVariant}>
            <FormLabel>Password</FormLabel>
            <TextField
              type="password"
              value={form.password.value}
              onChange={(event) => updatePassword(event.target.value)}
              error={form.password.touched && !form.password.valid}
              helperText={
                form.password.touched && !form.password.valid
                  ? form.password.error
                  : ""
              }
            />
          </FormControl>
          <FormControl variant={fieldVariant}>
            <FormLabel>Phone Number</FormLabel>
            <TextField
              type="text"
              sx={{ marginBottom: '20px' }}
              value={form.phoneNumber.value}
              onChange={(event) => updatePhoneNumber(event.target.value)}
              error={form.phoneNumber.touched && !form.phoneNumber.valid}
              helperText={
                form.phoneNumber.touched && !form.phoneNumber.valid
                  ? form.phoneNumber.error
                  : ""
              }
            />
          </FormControl>
          <Button
            variant="contained"
            type="submit"
            onClick={helper}
            disabled={
              !form.username.valid ||
              !form.email.valid ||
              !form.password.valid ||
              !form.phoneNumber.valid
            }
          >
            Register
          </Button>
          {/* </Stack> */}

        </Box>
        {/* </Box> */}
      </Container >
    </>
  );
}

RegisterView.propTypes = {
  form: PropTypes.object.isRequired,
  updateUsername: PropTypes.func.isRequired,
  updateEmail: PropTypes.func.isRequired,
  updatePhoneNumber: PropTypes.func.isRequired,
  updateFirstName: PropTypes.func.isRequired,
  updateLastName: PropTypes.func.isRequired,
  updatePassword: PropTypes.func.isRequired,
  helper: PropTypes.func.isRequired,
};

export default RegisterView;
