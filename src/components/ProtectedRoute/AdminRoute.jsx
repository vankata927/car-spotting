import { useContext } from "react";
import { Navigate } from "react-router-dom";
import PropTypes from "prop-types";
import AppContext from "../../contexts/AppContext";

const AdminRoute = ({ children }) => {
  const { appState } = useContext(AppContext);
  const { userData } = appState;

  if (!userData || userData.role !== "admin") {
    return <Navigate to="/" />;
  }

  return children;
};

AdminRoute.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AdminRoute;
