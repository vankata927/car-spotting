import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import AppContext from "../../contexts/AppContext";
import PropTypes from "prop-types";

const ProtectedRoute = ({ children }) => {
  const navigate = useNavigate();
  const { appState } = useContext(AppContext);

  useEffect(() => {
    if (appState.user === null) {
      navigate("/login");
    }
  }, [appState.user, navigate]);

  return children || null;
};
ProtectedRoute.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ProtectedRoute;
