import { useNavigate } from "react-router-dom";
import AppContext from "../../contexts/AppContext";
import { useContext } from "react";
import PropTypes from "prop-types";
import { ListItem, ListItemText } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";

const ProtectedButton = ({ to, children }) => {
  const navigate = useNavigate();
  const { appState } = useContext(AppContext);

  const handleClick = (e) => {
    if (!appState.user) {
      e.preventDefault();
      navigate("/login");
    }
  };

  return (
    <ListItem
      button
      component={RouterLink}
      to={to}
      onClick={appState.user ? undefined : handleClick}
    >
      <ListItemText primary={children} />
    </ListItem>
  );
};

ProtectedButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
export default ProtectedButton;
