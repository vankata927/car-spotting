import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getDatabase, ref, get } from "firebase/database";
import { Button, Typography, Box, Grid, Container } from "@mui/material";
import AppContext from "../contexts/AppContext";
import FeaturesGrid from "./HomeSubComponents/FeaturesGrid";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";

const HomeView = () => {
  const { appState } = useContext(AppContext);
  const navigate = useNavigate();
  const [userCount, setUserCount] = useState(0);

  useEffect(() => {
    const db = getDatabase();
    const usersRef = ref(db, "users");

    get(usersRef)
      .then((snapshot) => {
        if (snapshot.exists()) {
          setUserCount(snapshot.val() ? Object.keys(snapshot.val()).length : 0);
        } else {
          console.log("No data available");
        }
      })
      .catch((error) => {
        console.error(error);
      });

    if (appState?.userData) {
      navigate("/user/:username");
    }
  }, [appState, navigate]);

  return (
    <Box sx={{ px: "100px", py: "60px" }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div
            style={{
              position: "relative",
              background: "black",
              overflow: "hidden",
              height: "600px",
            }}
          >
            <Box
              component="img"
              sx={{
                height: "600px",
                width: "100%",
                objectFit: "cover",
                objectPosition: "0 0",
                opacity: "0.7",
              }}
              alt="The house from the offer."
              src="https://images.unsplash.com/photo-1580446623001-3abf670c5c55?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1790&q=80"
            />
            <div
              style={{
                width: "100%",

                position: "absolute",
                color: "white",
                top: "60%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                textAlign: "center",
                fontSize: "50px",
              }}
            >
              Unlock Your Passion, Embrace the Drive
              <br />
              <br />
              <div
                style={{
                  padding: "20px",
                  fontSize: "15pt",
                  opacity: "0.7",
                }}
              >
                Join {userCount} other enthusiasts on a journey through
                automotive discovery, one car at a time.
              </div>
              <Button
                variant="contained"
                onClick={() => navigate("/register")}
                sx={{ minWidth: 200 }}
              >
                Join Now
              </Button>
              <Typography
                style={{
                  opacity: "0.7",
                }}
              >
                Discover the experience
              </Typography>
              <ArrowDownwardIcon
                sx={{ position: "relative", marginBottom: "-40px" }}
              />
            </div>
          </div>
        </Grid>
        <Box backgroundColor="#F2F2F2" marginTop="60px">
          <Grid item xs={12}>
            <FeaturesGrid />
          </Grid>
        </Box>
        <Grid item xs={12}>
          <Container
            component="section"
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              my: 9,
            }}
          >
            <Box
              sx={{
                border: "4px solid currentColor",
                borderRadius: 0,
                height: "auto",
                py: 2,
                px: 5,
              }}
            >
              <Typography variant="h6" component="span">
                Already a member?
              </Typography>
            </Box>
            <Typography variant="subtitle1" sx={{ my: 3 }}>
              Welcome back and enjoy your stay!
            </Typography>
            <Button
              onClick={() => navigate("/login")}
              variant="outlined"
              color="primary"
            >
              Log In
            </Button>
          </Container>
        </Grid>
      </Grid>
    </Box>
  );
};

export default HomeView;
