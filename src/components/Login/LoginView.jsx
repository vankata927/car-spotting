import PropTypes from "prop-types";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Box, Stack } from "@mui/material";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import { Helmet } from "react-helmet-async";
import "./LoginView.css";
import { Link } from "react-router-dom";

function LoginView({
  // typographyVariant,
  fieldMargin,
  formData,
  handleChange,
  handleSubmit,
  loading,
  error,
}) {
  return (
    <>
      <Helmet>
        <title>Car Spotting</title>
        <body className="log-in-page" />
      </Helmet>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "white",
            padding: "20px",
            borderRadius: "5px",
          }}
        >
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          {error && <p>{error}</p>}
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <TextField
              name="email"
              onChange={handleChange}
              label="Email"
              required
              margin={fieldMargin}
              fullWidth
              value={formData.email}
            />
            <TextField
              name="password"
              onChange={handleChange}
              label="Password"
              required
              type="password"
              margin={fieldMargin}
              fullWidth
              value={formData.password}
            />
            <Stack direction="row" spacing={2} marginTop={2}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                disabled={loading}
                fullWidth
              >
                {loading ? "Logging in..." : "Login"}
              </Button>
            </Stack>
          </Box>

          <Grid container>
            <Grid item>
              <Link to="/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
}

LoginView.propTypes = {
  typographyVariant: PropTypes.string.isRequired,
  fieldMargin: PropTypes.string.isRequired,
  isSmallScreen: PropTypes.bool,
  formData: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  navigate: PropTypes.func.isRequired,
};

export default LoginView;
