import { useContext, useState, useEffect } from "react";
import { signInWithEmailAndPassword } from "firebase/auth";
import { authenticate, db } from "../../firebase/firebase-config";
import { useNavigate } from "react-router-dom";
import AppContext from "../../contexts/AppContext";
import LoginView from "./LoginView";
import { ref, get, query, orderByChild, equalTo } from "firebase/database";
function Login() {
  const { appState, setAppState } = useContext(AppContext);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const navigate = useNavigate();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (appState.user && appState.userData && appState.userData.handle) {
      navigate(`/user/${appState.userData.handle}`);
    }
  }, [appState, navigate]);

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setError(null);

    try {
      const userCredential = await signInWithEmailAndPassword(
        authenticate,
        formData.email,
        formData.password
      );

      const user = userCredential.user;
      if (user) {
        const usersRef = ref(db, "users");
        const userQuery = query(
          usersRef,
          orderByChild("uid"),
          equalTo(user.uid)
        );
        const snapshot = await get(userQuery);

        if (snapshot.exists()) {
          const userObj = Object.values(snapshot.val())[0];
          const userHandle = userObj.handle;

          setAppState((prevState) => ({
            ...prevState,
            user: {
              email: user.email,
              uid: user.uid,
            },
            userData: userObj, // Update the userData with the fetched user data
          }));

          // Navigate to the user's handle page
          navigate(`/user/${userHandle}`);
        } else {
          setError("User data not found. Please complete your profile.");
        }
      } else {
        setError("Login failed. Please check your credentials.");
      }
    } catch (error) {
      console.error("Login error:", error);
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <LoginView
      typographyVariant="h5"
      fieldMargin="normal"
      formData={formData}
      handleChange={handleChange}
      handleSubmit={handleSubmit}
      loading={loading}
      error={error}
      navigate={navigate}
    />
  );
}

export default Login;
