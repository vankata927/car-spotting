import { useContext, useState } from "react";
import { db, storage } from "../../firebase/firebase-config"; // Ensure you have these imports set up
import { ref, set } from "firebase/database";
import {
  uploadBytesResumable,
  getDownloadURL,
  ref as storageRef,
} from "firebase/storage";
import { Button, Typography, Box, Paper, TextField } from "@mui/material";
import AppContext from "../../contexts/AppContext";
import { useNavigate } from "react-router-dom";

function EditProfile() {
  const { appState } = useContext(AppContext);
  const [image, setImage] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImage(file);
    }
  };

  const handleUpload = async () => {
    if (!image) {
      setMessage("No image selected for upload.");
      return;
    }
    const storageReference = storageRef(
      storage,
      `profilePictures/${appState.user.uid}`
    );
    const uploadTask = uploadBytesResumable(storageReference, image);

    setUploading(true);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // You can add progress functionality here
      },
      (error) => {
        console.error("Error uploading image:", error);
        setMessage("Error uploading image. Please try again.");
        setUploading(false);
      },
      async () => {
        const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
        await set(
          ref(db, `users/${appState.userData.handle}/profilePictureURL`),
          downloadURL
        );
        setUploading(false);
        setMessage("Profile picture uploaded successfully!");
        navigate("/profile"); // Navigate back to the profile after successful upload
      }
    );
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mt: 3,
      }}
    >
      <Paper elevation={3} sx={{ padding: 3, width: "80%", maxWidth: 500 }}>
        <Typography variant="h4" gutterBottom>
          Edit Profile
        </Typography>
        <TextField
          variant="outlined"
          type="file"
          onChange={handleImageChange}
          fullWidth
          margin="normal"
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleUpload}
          disabled={uploading}
        >
          {uploading ? "Uploading..." : "Upload Profile Picture"}
        </Button>
        {message && (
          <Typography variant="body1" style={{ marginTop: "16px" }}>
            {message}
          </Typography>
        )}
      </Paper>
    </Box>
  );
}

export default EditProfile;
