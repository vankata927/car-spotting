import Profile from "./Profile";
import MyUploads from "./MyUploads";
import { Box, Typography } from "@mui/material";
import "./ProfileView.css";

function ProfileView() {
  return (
    <Box className="profile-view-container">
      <Profile />
      <Typography variant="h4" gutterBottom className="profile-title">
        My Uploaded Photos
      </Typography>
      <MyUploads />
    </Box>
  );
}

export default ProfileView;
