import { useContext, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Grid, Button } from "@mui/material";
import AppContext from "../../contexts/AppContext";
import {
  fetchUserPhotos,
  handleDeletePhoto,
} from "../../services/photo-services";

function MyUploads() {
  const { appState } = useContext(AppContext);
  const [userPhotos, setUserPhotos] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const getUserPhotos = async () => {
      const photos = await fetchUserPhotos(appState.userData.handle);
      setUserPhotos(photos);
    };

    getUserPhotos();
  }, [appState]);

  const deletePhoto = async (photo) => {
    // Log the photo object for debugging purposes
    console.log("Deleting photo:", photo);

    const isDeleted = await handleDeletePhoto(photo, appState.user.uid);
    if (isDeleted) {
      setUserPhotos((prevPhotos) =>
        prevPhotos.filter((p) => p.id !== photo.id)
      );
    } else {
      alert("Failed to delete photo. Please try again.");
    }
  };

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        style={{ marginBottom: "20px" }}
        onClick={() => navigate("/upload")}
      >
        Upload Photo
      </Button>
      <Grid container spacing={3}>
        {userPhotos.map((photo) => (
          <Grid item xs={12} sm={4} key={photo.photoURL}>
            <img
              src={photo.photoURL}
              alt={photo.headline}
              style={{ width: "100%", borderRadius: "5px" }}
            />
            <Button onClick={() => deletePhoto(photo)} color="secondary">
              Delete
            </Button>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

export default MyUploads;
