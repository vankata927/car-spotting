import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ref, get } from "firebase/database";
import { db } from "../../firebase/firebase-config";
import AppContext from "../../contexts/AppContext";
import { Box, Typography, Paper, Button } from "@mui/material";

function Profile() {
  const { appState } = useContext(AppContext);
  const [userData, setUserData] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const userHandle = appState?.userData?.handle;
        if (!userHandle) {
          console.error("Handle is missing from appState");
          return;
        }

        const userRef = ref(db, `users/${userHandle}`);
        const snapshot = await get(userRef);
        if (snapshot.exists()) {
          setUserData(snapshot.val());
        } else {
          console.error("User does not exist");
        }
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    fetchUserData();
  }, [appState]);

  if (!userData) return <Typography variant="h6">Loading...</Typography>;

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        mt: 3,
      }}
    >
      <Paper elevation={3} sx={{ padding: 3, width: "80%", maxWidth: 500 }}>
        {/* Display profile picture */}
        {userData.profilePictureURL ? (
          <img
            src={userData.profilePictureURL}
            alt="Profile"
            style={{
              width: "150px",
              borderRadius: "50%",
              marginBottom: "20px",
            }}
          />
        ) : (
          <Typography variant="body1">No profile picture</Typography>
        )}

        <Typography variant="h4" gutterBottom>
          {userData.handle}'s Profile
        </Typography>
        <Typography variant="body1">
          <strong>Email:</strong> {userData.email}
        </Typography>
        <Typography variant="body1">
          <strong>First Name:</strong> {userData.firstname}
        </Typography>
        <Typography variant="body1">
          <strong>Last Name:</strong> {userData.lastname}
        </Typography>
        <Typography variant="body1">
          <strong>Phone Number:</strong> {userData.phoneNumber}
        </Typography>
        {/* Add other fields as needed */}
        <Button
          variant="contained"
          color="primary"
          onClick={() => navigate("/edit-profile")}
        >
          Edit Profile
        </Button>
      </Paper>
    </Box>
  );
}

export default Profile;
