import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useEffect } from "react";

import { useNavigate } from "react-router-dom";
import { ref, get, update } from "firebase/database";
import { db } from "../firebase/firebase-config";

const VerifyEmail = () => {
  const navigate = useNavigate();
  const auth = getAuth();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, async (user) => {
      if (user?.emailVerified) {
        const userRef = ref(db, `users/${user.uid}`);
        const snapshot = await get(userRef);
        if (snapshot.exists()) {
          await update(userRef, { role: "user" });
        }
        navigate("/");
      }
    });

    return unsubscribe;
  }, [navigate, auth]);

  return (
    <div>
      Please verify your email and sign in again. Check your email for a
      verification link.
    </div>
  );
};

export default VerifyEmail;
