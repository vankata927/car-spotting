import { useState, useContext } from "react";
import { Link as RouterLink, useLocation } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  IconButton,
  Box,
  Drawer,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import LogoutIcon from "@mui/icons-material/Logout";
import { styled } from "@mui/system";
import AppContext from "../contexts/AppContext";
import { useNavigate } from "react-router-dom";
import ProtectedButton from "./ProtectedRoute/ProtectedButton";
import { handleLogout } from "../services/auth-services";

const StyledAppBar = styled(AppBar)({
  background: "black",
  marginBottom: "40px",
  position: "fixed",
  top: 0,
  left: 0,
  right: 0,
  zIndex: "1000",
});

const Navbar = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const handleDrawerOpen = () => setDrawerOpen(true);
  const handleDrawerClose = () => setDrawerOpen(false);

  const { appState, logout } = useContext(AppContext);
  const location = useLocation();
  const navigate = useNavigate();

  const performLogout = () => handleLogout(logout, navigate);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <StyledAppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" onClick={handleDrawerOpen}>
            <MenuIcon />
          </IconButton>
          <Button
            color="inherit"
            component={RouterLink}
            to={appState.user ? "/dashboard" : "/"}
            sx={{ flexGrow: 0.05 }}
          >
            <Typography variant="h6" component="div">
              Car Spotting
            </Typography>
          </Button>
          <Box sx={{ flexGrow: 1 }} /> {/* This line added */}
          {appState.user ? (
            <IconButton color="inherit" onClick={performLogout}>
              <LogoutIcon />
            </IconButton>
          ) : (
            location.pathname !== "/login" && (
              <Button color="inherit" component={RouterLink} to="/login">
                Login
              </Button>
            )
          )}
        </Toolbar>
      </StyledAppBar>
      <Drawer anchor="left" open={drawerOpen} onClose={handleDrawerClose}>
        <List>
          {appState.userData && (
            <ListItem
              button
              component={RouterLink}
              to={appState.userData ? `/user/${appState.userData.handle}` : "#"}
            >
              <ListItemText
                primary={
                  appState.userData ? appState.userData.handle : "Loading..."
                }
              />
            </ListItem>
          )}
          {appState.user &&
            appState.userData &&
            appState.userData.role === "admin" && (
              <>
                <ListItem button component={RouterLink} to="/admin">
                  <ListItemText primary="Users" />
                </ListItem>
                <ListItem button component={RouterLink} to="/admin/list1">
                  <ListItemText primary="All Goals" />
                </ListItem>
              </>
            )}
          <ProtectedButton to="/upload">Upload</ProtectedButton>
          <ProtectedButton to="/photos">Photos</ProtectedButton>
          <ProtectedButton
            to={
              appState.userData
                ? `users/${appState.userData.handle}/favorites`
                : "#"
            }
          >
            Favorites
          </ProtectedButton>

          <ProtectedButton to="/data">CarData</ProtectedButton>
          <ProtectedButton to="/community">Community</ProtectedButton>
          <ListItem button component={RouterLink} to="/about">
            <ListItemText primary="About" />
          </ListItem>
        </List>
      </Drawer>
    </Box>
  );
};

export default Navbar;
