import Divider from "@mui/material/Divider";
import CameraIcon from "@mui/icons-material/PhotoCamera";
import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";

const getImageByIndex = (index) => {
  if (index === 0) {
    return "https://media.licdn.com/dms/image/D4D03AQEY-hPUTzHnbQ/profile-displayphoto-shrink_800_800/0/1684659918543?e=1700092800&v=beta&t=ZytbGmx1o579CIKAzU7o83mUXaVVa7eStjW10dSdYmw"; // Replace with your Georgi's photo
  }
};
const cards = [1];

const defaultTheme = createTheme();

const About = () => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <CameraIcon sx={{ mr: 2 }} />

      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: "background.paper",
            pt: 8,
            pb: 6,
            alignItems: "center",
          }}
        >
          <Container maxWidth="100%">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              color="text.primary"
              gutterBottom
              noWrap
            >
              Meet the person behind Car Spotting
            </Typography>
            <Divider />
            <Typography
              variant="h5"
              align="center"
              color="text.secondary"
              paragraph
              sx={{ mr: 2, px: "100px" }}
            >
              Hello! I worked and had fun to make this project idea into a real
              functioning website! I hope you like what result.
            </Typography>
          </Container>
        </Box>
        <Container sx={{ py: 8 }} maxWidth="lg">
          {/* End hero unit */}
          <Grid container spacing={2}>
            {cards.map((card, index) => (
              <Grid item key={card} xs={12} sm={6} md={4}>
                <Card
                  sx={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <CardMedia
                    component="div"
                    sx={{
                      // 16:9
                      pt: "100%",
                    }}
                    image={getImageByIndex(index)}
                  />
                  <CardContent sx={{ flexGrow: 2 }}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {index === 0 ? "Ivan Dimchev" : ""}
                    </Typography>
                    <Divider />
                    <Typography mt="20px">
                      {index === 0
                        ? "Prepare to meet Ivan, the coding maverick with a knack for transforming intricate lines of code into mind-blowing websites."
                        : ""}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <a
                      href="https://github.com/IvanDimchev"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <IconButton size="small">
                        <GitHubIcon />
                      </IconButton>
                    </a>
                    <a
                      href="https://www.linkedin.com/in/ivan-dimchev-014a87277/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <IconButton size="small">
                        <LinkedInIcon />
                      </IconButton>
                    </a>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
    </ThemeProvider>
  );
};

export default About;
