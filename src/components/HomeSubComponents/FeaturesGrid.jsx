import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import StarIcon from "@mui/icons-material/StarBorder";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import GlobalStyles from "@mui/material/GlobalStyles";
import Container from "@mui/material/Container";

const tiers = [
  {
    title: "Perfect your workout form ",
    price: "0",
    description: [
      "Find more than 100 pictures and learn about the story and specifics of these incredible cars",
    ],
    image:
      "https://images.unsplash.com/photo-1517153192978-b2e379ac0710?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
  },
  {
    title: "Favorite cars",
    price: "15",
    description: [
      "Share your favorite car pictures and help other users see these magnificent pieces of art!",
    ],
    image:
      "https://images.unsplash.com/photo-1646562501065-a8e567ad95f3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80",
  },
  {
    title: "Stay on track together",
    price: "30",
    description: [
      "Join a friendly community of like-minded people and participate in challenges together!",
    ],
    image:
      "https://images.unsplash.com/photo-1606138369223-fb3a260d9b99?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2074&q=80",
  },
];

const FeaturesGrid = () => {
  return (
    <>
      <GlobalStyles
        styles={{ ul: { margin: 0, padding: 0, listStyle: "none" } }}
      />
      <CssBaseline />

      {/* Hero unit */}
      <Container
        disableGutters
        maxWidth="xl"
        component="main"
        sx={{ pt: 8, pb: 6 }}
      >
        <Typography
          variant="h5"
          align="center"
          color="text.secondary"
          component="p"
        >
          With CarSpotting's advanced features and user-friendly design,
          cataloging your automotive encounters has never been this effortless.
          Bid farewell to traditional car diaries and embrace the future with
          real-time updates, custom car galleries, and the thrill of connecting
          with fellow enthusiasts. Dive into a world where every ride you spot
          becomes a lasting memory.
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="lg" component="main">
        <Grid container spacing={5} alignItems="flex-end" paddingBottom="50px">
          {tiers.map((tier) => (
            // Enterprise card is full width at sm breakpoint
            <Grid
              item
              key={tier.title}
              xs={12}
              sm={tier.title === "Enterprise" ? 12 : 6}
              md={4}
            >
              <Card>
                <CardHeader
                  title={tier.title}
                  subheader={tier.subheader}
                  titleTypographyProps={{ align: "center" }}
                  action={tier.title === "Pro" ? <StarIcon /> : null}
                  subheaderTypographyProps={{
                    align: "center",
                  }}
                  sx={{
                    backgroundColor: (theme) =>
                      theme.palette.mode === "light"
                        ? theme.palette.grey[300]
                        : theme.palette.grey[700],
                  }}
                />
                <CardContent>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "baseline",
                      mb: 2,
                    }}
                  ></Box>
                  <ul>
                    <CardMedia
                      component="img"
                      sx={{
                        alignContent: "center",
                        position: "relative",
                        objectFit: "cover",
                        width: "100%",
                        height: "200px",
                      }}
                      image={tier.image} // Set the image URL for the card
                      alt={tier.title}
                    />
                    {tier.description.map((line) => (
                      <Typography
                        py="30px"
                        component="li"
                        variant="subtitle1"
                        align="center"
                        key={line}
                      >
                        {line}
                      </Typography>
                    ))}
                  </ul>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </>
  );
};

export default FeaturesGrid;
