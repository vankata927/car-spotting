import { createContext } from "react";

const AppContext = createContext({
  appState: null,
  setAppState: () => {},
});

export default AppContext;
